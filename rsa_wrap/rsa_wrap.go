package rsa_wrap

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"
)

// 生成RSA密钥对
func GenerateKeyPair(bits int) (*rsa.PrivateKey, error) {
	return rsa.GenerateKey(rand.Reader, bits)
}

// 使用公钥加密数据
func Encrypt(publicKey *rsa.PublicKey, plaintext []byte) ([]byte, error) {
	label := []byte("") // OAEP的“label”参数（可以留空）
	return rsa.EncryptOAEP(sha256.New(), rand.Reader, publicKey, plaintext, label)
}

// 使用私钥解密数据
func Decrypt(privateKey *rsa.PrivateKey, ciphertext []byte) ([]byte, error) {
	label := []byte("") // OAEP的“label”参数（应该与加密时使用的相同）
	return rsa.DecryptOAEP(sha256.New(), rand.Reader, privateKey, ciphertext, label)
}

// 将私钥保存到PEM文件
func SavePrivateKey(privateKey *rsa.PrivateKey, filename string) error {
	outFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer outFile.Close()
	privateKeyPEM := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(privateKey),
	}
	return pem.Encode(outFile, privateKeyPEM)
}

// 将公钥保存到PEM文件
func SavePublicKey(publicKey *rsa.PublicKey, filename string) error {
	outFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer outFile.Close()
	publicKeyBytes, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return err
	}
	publicKeyPEM := &pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: publicKeyBytes,
	}
	return pem.Encode(outFile, publicKeyPEM)
}

// 从PEM文件加载公钥
func LoadPublicKey(filename string) (*rsa.PublicKey, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(data)
	if block == nil || block.Type != "RSA PUBLIC KEY" {
		return nil, fmt.Errorf("failed to decode PEM block containing public key")
	}

	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	switch pub := pub.(type) {
	case *rsa.PublicKey:
		return pub, nil
	default:
		return nil, fmt.Errorf("unknown public key type in PEM block")
	}
}

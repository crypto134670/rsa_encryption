package main

import (
	"fmt"
	"os"
	"rsa_encryption/rsa_wrap"
)

// 读取文件内容
func readFile(filename string) ([]byte, error) {
	return os.ReadFile(filename)
}

// 写入文件内容
func writeFile(filename string, data []byte) error {
	return os.WriteFile(filename, data, 0644)
}

func main() {
	// 生成密钥对
	privateKey, err := rsa_wrap.GenerateKeyPair(2048)
	if err != nil {
		fmt.Println(err)
		return
	}
	// 保存私钥
	if err := rsa_wrap.SavePrivateKey(privateKey, "private_key.pem"); err != nil {
		fmt.Println(err)
		return
	}
	if err := rsa_wrap.SavePublicKey(&privateKey.PublicKey, "public_key.pem"); err != nil {
		fmt.Println(err)
		return
	}
	// 读取明文文件内容
	plaintext, err := readFile("plaintext.txt")
	if err != nil {
		fmt.Println("Error reading plaintext file:", err)
		return
	}

	// 加载公钥进行加密
	publicKey, err := rsa_wrap.LoadPublicKey("public_key.pem")
	if err != nil {
		fmt.Println("Error loading public key:", err)
		return
	}

	// 使用公钥加密明文
	ciphertext, err := rsa_wrap.Encrypt(publicKey, plaintext)
	if err != nil {
		fmt.Println("Error encrypting plaintext:", err)
		return
	}

	// 将密文写入ciphertext.txt文件
	if err := writeFile("ciphertext.txt", ciphertext); err != nil {
		fmt.Println("Error writing ciphertext file:", err)
		return
	}

	// 读取密文文件内容
	ciphertextFromFile, err := readFile("ciphertext.txt")
	if err != nil {
		fmt.Println("Error reading ciphertext file:", err)
		return
	}

	// 使用私钥解密密文
	decryptedText, err := rsa_wrap.Decrypt(privateKey, ciphertextFromFile)
	if err != nil {
		fmt.Println("Error decrypting ciphertext:", err)
		return
	}

	// 将解密后的明文写入result.txt文件
	if err := writeFile("result.txt", decryptedText); err != nil {
		fmt.Println("Error writing result file:", err)
		return
	}

	fmt.Println("Encryption and decryption successful!")
}

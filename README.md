## 运行命令

编译运行

```bash
go build
./rsa_encryption
```

直接运行

```bash
go run main.go
```

自动完成生成公钥/密钥对，保存和加载公钥/密钥文件，加载明文，加密，保存密文，解密的操作。